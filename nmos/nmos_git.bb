SUMMARY = "NMOS - An NMOS (Networked Media Open Specifications) Registry and Node in C++"
SECTION = "NMOS products"
LICENSE = "CLOSED"

DEPENDS += " \
    boost \
    cpprest \
    websocketpp \
    avahi \
	"

SRC_URI = " \
    gitsm://github.com/sony/nmos-cpp.git;protocol=https \
	"

S = "${WORKDIR}/git/Development"
SRCREV = "41d983cee3e416470ac4e62ff92b059bfc1ecdc7"

EXTRA_OECMAKE += " -DCMAKE_BUILD_TYPE=Release -DUSE_CONAN=0 -DBONJOUR_INCLUDE=${WORKDIR}/recipe-sysroot/usr/include/avahi-compat-libdns_sd/"

do_install_append() {
    install -d ${D}/opt/app/
    install -m 777 ./nmos-cpp-node ${D}/opt/app/
}

FILES_${PN} += "\
    /opt/ \
    "

inherit cmake
